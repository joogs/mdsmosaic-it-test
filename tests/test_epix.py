import os
import time

import pytest
import requests
import zeep.exceptions
from requests import RequestException

from mdsmosaic_ci_test import epix


@pytest.fixture
def epix_client():
    epix_wsdl_url = os.environ.get("IT_EPIX_WSDL_URL")

    if epix_wsdl_url is None:
        raise ValueError("E-PIX WSDL URL is not set")

    start_time = time.time()
    end_time = start_time + 10

    done = False

    while time.time() <= end_time:
        try:
            r = requests.get(epix_wsdl_url)

            if r.status_code == 200:
                done = True
                break
        except RequestException:
            time.sleep(1)

    if not done:
        raise ValueError("Failed to connect after 120 seconds")

    return epix.EPIXClient(epix_wsdl_url)


@pytest.mark.integration
def test_get(epix_client: epix.EPIXClient):
    with pytest.raises(zeep.exceptions.Fault) as excinfo:
        epix_client.get_identity_ids_in_domain("foo")

    assert "domain not found: foo" in str(excinfo.value)
