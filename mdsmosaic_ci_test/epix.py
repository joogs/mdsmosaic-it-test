from typing import List

from zeep import Client


def get_identity_ids_in_domain(client: Client, domain: str) -> List[int]:
    resp = client.service.getPersonsForDomain(domain)

    return [
        e.referenceIdentity.identityId for e in resp
    ]


class EPIXClient:

    def __init__(self, wsdl_url: str):
        self.client = Client(wsdl_url)

    def get_identity_ids_in_domain(self, domain: str) -> List[int]:
        return get_identity_ids_in_domain(self.client, domain)
